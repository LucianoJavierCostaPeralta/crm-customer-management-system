# CRM: Customer Management Systen

## Install

```bash
  git clone git@gitlab.com:LucianoJavierCostaPeralta/crm-customer-management-system.git
  or
  git clone https://gitlab.com/LucianoJavierCostaPeralta/crm-customer-management-system.git

  cd crm-customer-management-system

  cp .env.example .env.local

  yarn
  or
  npm install

  yarn dev
  or
  npm run dev

```

- Currently a fakeAPI created with [JSON Server](https://www.npmjs.com/package/json-server) is used.

- Install

```bash

  npm install -g json-server

```

- Run JSON Server

```bash

  json-server --watch db.json

```

### [Demo](https://crm-customer-management-system.vercel.app/)
