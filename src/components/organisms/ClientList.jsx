import { Form, redirect, useNavigate } from "react-router-dom";
import { deleteClient } from "../../api/Clients";

export const action = async ({ params }) => {
  const idClient = params.id;

  deleteClient(idClient);

  return redirect("/");
};

export const ClientList = ({ data }) => {
  const navigate = useNavigate();

  const handleNavigate = (id) => navigate(`/client/${id}/edit`);

  return (
    <table className="w-full bg-white shadow mt-5 table-auto table">
      <thead className="bg-blue-800 text-white text-xl">
        <tr>
          <th className="p-3">Client</th>
          <th className="p-3">Contact</th>
          <th className="p-3">Actions</th>
        </tr>
      </thead>
      <tbody>
        {data.map((item, index) => {
          return (
            <tr key={index} className="border-b">
              <td className="p-4">
                {item.name && (
                  <p className="text-2xl text-gray-800 font-black">
                    {item.name}
                  </p>
                )}
                {item.company && (
                  <p className="mt-2 text-xl text-gray-600 font-semibold">
                    {item.company}
                  </p>
                )}
              </td>
              <td className="p-4">
                {item.email && (
                  <p className="text-gray-600 font-medium text-xl">
                    <span className="text-gray-800 font-bold uppercase">
                      Email:&nbsp;
                    </span>
                    {item.email}
                  </p>
                )}
                {item.phone && (
                  <p className="text-gray-600 mt-2 font-medium text-xl">
                    <span className="text-gray-800 font-bold uppercase">
                      Phone:&nbsp;
                    </span>
                    {item.phone}
                  </p>
                )}
              </td>

              <td className="p-4 flex gap-5 justify-center items-center">
                <button
                  type="button"
                  className="bg-blue-600 text-white hover:bg-blue-700
                  uppercase font-bold text-xs py-3 px-5 "
                  onClick={() => handleNavigate(item.id)}
                >
                  Edit
                </button>
                <Form
                  method="post"
                  action={`/client/${item.id}/destroy`}
                  onSubmit={(e) => {
                    if (
                      !confirm("Are you sure you want to delete the client?")
                    ) {
                      e.preventDefault();
                    }
                  }}
                >
                  <button
                    type="submit"
                    className="bg-red-600 text-white hover:bg-red-700
                  uppercase font-bold text-xs py-3 px-5 "
                  >
                    Delete
                  </button>
                </Form>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};
