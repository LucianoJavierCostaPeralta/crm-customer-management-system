export const Error = ({ children }) => {
  return (
    <div className="p-4">
      <div
        className="bg-red-600 text-center text-xl text-white my-5 font-bold
        p-3 "
      >
        {children}
      </div>
    </div>
  );
};
