import React from "react";

export const FormClient = ({ client }) => {
  return (
    <>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-4">
        <div className="p-4">
          <label className="text-gray-800" htmlFor="name">
            Name
          </label>
          <input
            id="name"
            type="text"
            className="mt-2 block w-full p-3 bg-gray-50"
            placeholder="Name"
            name="name"
            defaultValue={client?.name}
          />
        </div>
        <div className="p-4">
          <label className="text-gray-800" htmlFor="company">
            Company
          </label>
          <input
            id="company"
            type="text"
            className="mt-2 block w-full p-3 bg-gray-50"
            placeholder="Company"
            name="company"
            defaultValue={client?.company}
          />
        </div>
        <div className="p-4">
          <label className="text-gray-800" htmlFor="email">
            E-mail
          </label>
          <input
            id="email"
            type="email"
            className="mt-2 block w-full p-3 bg-gray-50"
            placeholder="Email"
            name="email"
            defaultValue={client?.email}
          />
        </div>
        <div className="p-4">
          <label className="text-gray-800" htmlFor="telefono">
            Phone
          </label>
          <input
            id="phone"
            type="tel"
            className="mt-2 block w-full p-3 bg-gray-50"
            placeholder="Phone"
            name="phone"
            defaultValue={client?.phone}
          />
        </div>
      </div>
      <div className="grid grid-cols-1  gap-4 mb-4">
        <div className="p-4">
          <label className="text-gray-800" htmlFor="note">
            Note
          </label>
          <textarea
            as="textarea"
            id="note"
            type="text"
            className="mt-2 block w-full p-3 bg-gray-50 h-40 align-self"
            placeholder="Description"
            name="note"
            defaultValue={client?.note}
          />
        </div>
      </div>
      <div className="p-4">
        <input
          type="submit"
          value="Register client"
          className="w-full text-lg mt-5 bg-blue-800 hover:bg-blue-900
            p-3 text-white uppercase font-bold cursor-pointer"
        />
      </div>
    </>
  );
};
