import React from "react";
import { Link, useLocation } from "react-router-dom";

export const NavBar = () => {
  const location = useLocation();

  const styleLink = "text-2xl hover:text-blue-300";

  const variantHome =
    location.pathname === "/" ? "text-blue-300" : "text-white";

  const variantNewClient =
    location.pathname === "/client/new" ? "text-blue-300" : "text-white";

  return (
    <nav className="mt-10">
      <ul>
        <li>
          <Link to="/" className={`${variantHome} ${styleLink}`}>
            Clients
          </Link>
        </li>
        <li>
          <Link to="/client/new" className={`${variantNewClient} ${styleLink}`}>
            New client
          </Link>
        </li>
      </ul>
    </nav>
  );
};
