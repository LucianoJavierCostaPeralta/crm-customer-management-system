import { Form, Link, redirect, useActionData } from "react-router-dom";
import { addClient } from "../../api/Clients";
import { Error } from "../organisms/Error";
import { FormClient } from "../organisms/FormClient";

export const action = async ({ request }) => {
  const formData = await request.formData();

  const data = Object.fromEntries(formData);

  // Validation errors
  const errors = [];
  if (Object.values(data).includes("")) {
    errors.push("All fields are required");
  }

  // return errors
  if (Object.keys(errors).length) {
    return errors;
  }

  await addClient(data);

  return redirect("/");
};

export const NewClient = () => {
  const errors = useActionData();

  return (
    <>
      <h1 className="text-4xl font-black text-blue-700">New Clients</h1>
      <p className="mt-3 text-xl font-normal text-gray-500">
        Complete the fields to upload a client
      </p>

      <div className="flex start mt-10">
        <Link
          to="/"
          className="bg-blue-600 text-white py-3 px-5
          text-lg uppercase font-bold hover:bg-blue-700 "
        >
          Back to clients
        </Link>
      </div>

      <div className="bg-white px-5 py-10 mt-10 mx-auto shadow-md rounded-md">
        {errors?.length &&
          errors.map((err, i) => {
            return <Error key={i}>{err}</Error>;
          })}

        <Form method="POST">
          <FormClient />
        </Form>
      </div>
    </>
  );
};
