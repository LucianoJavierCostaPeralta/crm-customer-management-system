import React from "react";
import {
  Form,
  Link,
  redirect,
  useActionData,
  useLoaderData,
} from "react-router-dom";
import { getClient, updateClient } from "../../api/Clients";
import { Error } from "../organisms/Error";
import { FormClient } from "../organisms/FormClient";

export const loader = async ({ params }) => {
  const client = await getClient(params.id);

  if (Object.values(client).length === 0) {
    throw new Response("", {
      status: 404,
      statusText: "No results found",
    });
  }

  return client;
};

export const action = async ({ params, request }) => {
  const formData = await request.formData();

  const data = Object.fromEntries(formData);

  // Validation errors
  const errors = [];
  if (Object.values(data).includes("")) {
    errors.push("All fields are required");
  }

  // return errors
  if (Object.keys(errors).length) {
    return errors;
  }

  await updateClient(params.id, data);

  return redirect("/");
};

const Edit = () => {
  const client = useLoaderData();
  const errors = useActionData();

  return (
    <>
      <h1 className="text-4xl font-black text-blue-700">Edit Client</h1>

      <div className="flex start mt-10">
        <Link
          to="/"
          className="bg-blue-600 text-white py-3 px-5
      text-lg uppercase font-bold hover:bg-blue-700 "
        >
          Back to clients
        </Link>
      </div>

      <div className="bg-white px-5 py-10 mt-10 mx-auto shadow-md rounded-md">
        {errors?.length &&
          errors.map((err, i) => {
            return <Error key={i}>{err}</Error>;
          })}

        <Form method="POST">
          <FormClient client={client} />
        </Form>
      </div>
    </>
  );
};

export default Edit;
