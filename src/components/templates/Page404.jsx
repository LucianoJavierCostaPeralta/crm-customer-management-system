import React from "react";
import { useRouteError } from "react-router-dom";

const Page404 = () => {
  const error = useRouteError();

  console.log(error);

  return (
    <div className="flex flex-col items-center justify-center h-screen   p-4  ">
      <h1 className="text-5xl font-bold text-blue-800 mb-4">
        404 - Page not found
      </h1>
      <p className="text-2xl mt-5 bg-yellow-100 p-4 font-semibold  text-red-600 mb-8">
        {error.statusText || error.message}
      </p>
    </div>
  );
};

export default Page404;
