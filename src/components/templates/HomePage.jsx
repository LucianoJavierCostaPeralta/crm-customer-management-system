import { Link, useLoaderData } from "react-router-dom";
import { getClients } from "../../api/Clients";
import { ClientList } from "../organisms/ClientList";

export const loader = async () => {
  const clients = await getClients();

  return clients;
};

export const HomePage = () => {
  const data = useLoaderData();

  return (
    <>
      <h1 className="text-4xl font-black text-blue-700">Clients</h1>
      <p className="mt-3 text-xl font-normal text-gray-500">
        Manage your clients
      </p>
      {data.length ? (
        <ClientList data={data} />
      ) : (
        <div>
          <p
            className="text-center  mx-auto p-10
        font-bold text-xl mt-20 text-red-400 bg-yellow-100"
          >
            There are currently no customers on the list.
          </p>

          <div className="flex start mt-10">
            <Link
              to="/client/new"
              className="bg-blue-600 text-white py-3 px-5
      text-lg uppercase font-bold hover:bg-blue-700 "
            >
              Add client
            </Link>
          </div>
        </div>
      )}
    </>
  );
};
