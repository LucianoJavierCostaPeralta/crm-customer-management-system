const url = `${import.meta.env.VITE_API_URL}/clients`;

export const getClients = async () => {
  const res = await fetch(url);

  const data = await res.json();

  return data;
};

export const getClient = async (id) => {
  const res = await fetch(`${url}/${id}`);

  const data = await res.json();

  return data;
};

export const addClient = async (data) => {
  try {
    const res = await fetch(url, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
      },
    });

    await res.json();
  } catch (error) {
    console.log(error);
  }
};

export const updateClient = async (id, data) => {
  try {
    const res = await fetch(`${url}/${id}`, {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
      },
    });

    await res.json();
  } catch (error) {
    console.log(error);
  }
};

export const deleteClient = async (id) => {
  try {
    const res = await fetch(`${url}/${id}`, {
      method: "DELETE",
    });
  } catch (error) {
    console.log(error);
  }
};
