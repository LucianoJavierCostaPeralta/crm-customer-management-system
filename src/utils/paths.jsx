import Edit, {
  loader as editLoader,
  action as editClientAction,
} from "../components/templates/Edit";
import {
  HomePage,
  loader as clientLoader,
} from "../components/templates/HomePage";
import { Layout } from "../components/templates/Layout";
import {
  NewClient,
  action as newClientAction,
} from "../components/templates/NewClient";

import { action as deleteClientAction } from "../components/organisms/ClientList";
import Page404 from "../components/templates/Page404";

export const paths = [
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        index: true,
        element: <HomePage />,
        loader: clientLoader,
        errorElement: <Page404 />,
      },
      {
        path: "/client/new",
        element: <NewClient />,
        action: newClientAction,
        errorElement: <Page404 />,
      },
      {
        path: "/client/:id/edit",
        element: <Edit />,
        loader: editLoader,
        action: editClientAction,
        errorElement: <Page404 />,
      },
      {
        path: "/client/:id/destroy",
        action: deleteClientAction,
        errorElement: <Page404 />,
      },
    ],
  },
];
